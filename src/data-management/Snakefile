# subworkflow: data-management
#
# Contributors: guangchun

# --- Importing Configuration Files --- #

configfile: "config.yaml"

# --- Variable Declarations ---- #
runR = "Rscript --no-save --no-restore --verbose"
logAll = "2>&1"

# --- Build Rules --- #


rule filter_normalization:
    input:
        script = config["src_data_mgt"] + "filter-normalization.r",
        data = config["out_data"] + "cellranger.rds",
        params = config["src_data_mgt"]+ "param-filter-normalization.json"
    output:
        data = config["out_data"] + "normalized.rds"
    log:
        config["log"] + "data_preproc/filter-normalization.txt"
    shell:
        "{runR} {input.script} --data {input.data} --param {input.params} \
                --out {output.data} >{log} {logAll}"

rule load_cellranger:
    input:
        script = config["src_data_mgt"] + "load-cellranger.r",
        params = config["src_data_mgt"] + "param-load-cellranger.json"
    output:
        data = config["out_data"] + "cellranger.rds"
    log:
        config["log"] + "data_preproc/load-cellranger.txt"
    shell:
        "{runR} {input.script} --param {input.params} \
            --out {output.data}  > {log} {logAll}"

rule run_qc:
    input:
        script = config["src_data_mgt"] + "qc-doublets-viability.r",
        params = config["src_data_mgt"] + "qc-plotSize.json",
        data = config["out_data"] + "cellranger.rds",
    output:
        data = config["out_data"] + "qc-doublets-viability.pdf"
    log:
        config["log"] + "data_preproc/qc-doublets-viability.txt"
    shell:
        "{runR} {input.script} --param {input.params} \
            --out {output.data} --data {input.data} > {log} {logAll}"
