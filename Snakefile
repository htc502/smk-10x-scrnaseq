# Main Workflow - MRW Replication
# Contributors: @lachlandeer, @julianlanger

# --- Importing Configuration Files --- #

configfile: "config.yaml"

# --- Variable Declarations ---- #
runR = "Rscript --no-save --no-restore --verbose"
logAll = "2>&1"

# --- Workflow ---- #
subworkflow snn_markers:
    workdir: config["ROOT"]
    snakefile: config["src_analysis"] + "Snakefile"

# --- Main Build Rules --- #

rule all:
    input:
        markersPlot = snn_markers(config["out_analysis"] + "heatmap.png")

# --- Packrat Rules --- #

# packrat_install: installs packrat onto machine
rule packrat_install:
    shell:
        "R -e 'install.packages(\"packrat\", repos=\"http://cran.us.r-project.org\")'"

# packrat_install: initialize a packrat environment for this project
rule packrat_init:
    shell:
        "R -e 'packrat::init()'"

# packrat_snap   : Look for new R packages in files & archives them
rule packrat_snap:
    shell:
        "R -e 'packrat::snapshot()'"

# packrat_restore: Installs archived packages onto a new machine
rule packrat_restore:
    shell:
        "R -e 'packrat::restore()'"

# --- Cleaning Rules --- #

# clean_all      : delete all output and log files for this project
rule clean_all:
    shell:
        "rm -rf out/ log/ *.pdf *.html"

# clean_output   : delete all built files in project's output and ROOT directory
rule clean_output:
    shell:
        "rm -rf out/ *.pdf *.html"

# clean_logs     : delete all log files for this project
rule clean_log:
    shell:
        "rm -rf log/"

# --- Help Rules --- #

# help_main      : prints help comments for Snakefile in ROOT directory
rule help_main:
    input: "Snakefile"
    shell:
        "sed -n 's/^##//p' {input}"

# help_analysis  : prints help comments for Snakefile in analysis directory
rule help_analysis:
    input: config["src_analysis"] + "Snakefile"
    shell:
        "sed -n 's/^##//p' {input}"

# help_data_mgt  : prints help comments for Snakefile in data-management directory
rule help_data_mgt:
    input: config["src_data_mgt"] + "Snakefile"
    shell:
        "sed -n 's/^##//p' {input}"
